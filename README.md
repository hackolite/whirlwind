Whirlwind
=========

Reading Whisper database with Tornado.

Status
------

 * √ Async reading of whisper db
 * √ Carbon clone with sync writer
 * √ Querying db
 * _ Merging written and RAM values
 * _ Basic Http render API
 * _ Time seris computation
 * _ Javascript graphing
 * _ Metrics meta datas

Licence
-------

Apache Licence 2 © 2013 Mathieu Lecarme.

Large parts of code come from Graphite and Whisper projects,
under the same licence.
